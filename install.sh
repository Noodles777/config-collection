#! /bin/bash

update_repo=false

#Check if we got '-u' option from the command line
if [ "$1" == "-u" ]; then
    update_repo=true
fi

if [[ $EUID -eq 0 ]]; then
    echo "Running as root. Do you wish to install dependency packages using apt?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) apt-get install xcompmgr feh curl git; break;;
            No ) echo "No chosen, skipping package install"; break;;
            * ) echo "Invalid option"; continue;;
        esac
    done
else
    echo "Not running as root, cannot install dependency packages so skipping"
fi

#Get some pre-requisites if possible
#First check if git is available
if ! dpkg-query -l git > /dev/null; then
    if [[ $EUID -eq 0 ]]; then
        echo "Git is not installed, and is required do get some dependencies. Install it now?"
        select yesno in "Yes" "No"; do
            case $yesno in
                Yes ) apt-get install git; break;;
                No ) echo "You have chosen no, dependency install skipped"; break;;
                * ) echo "Invalid option"; continue;;
            esac
        done
    else
        echo "Git is not installed, but cannot be because the script is not run as root. Some extra dependencies will be skipped"
    fi
else
    echo "Git is installed and can be used to get some extra dependencies. Would you like to get them now?"
    select yesn in "Yes" "No"; do
        case $yesn in
            Yes ) git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim; break;;
            No ) echo "You have chosen not to install the extra dependencies. You can install them later manually"; break;;
            * ) echo "Invalid option"; continue;;
        esac
    done
fi

i=1
#Read the config file and prepare the commands
while read -r line
do
    word1=""
    word2=""
    wordselect=true

    #Split each line we got from the file by spaces
    for word in $line
    do
        if $wordselect; then
            word1="$word1$word"
        else
            word2="$word2$word"
        fi

        #Check if the last character of the string was an escape character
        #Used for escaping spaces in directory names
        if [ ${word:(-1)} == '\' ]; then
            if $wordselect; then
                word1="$word1 "
            else
                word2="$word2 "
            fi
        else
            wordselect=false
        fi
    done

    #The default command is cp
    cmd="cp"
    configs="./configs/"

    #The first path must have the config directory prepended to it
    word1="$configs$word1"

    #Create directories if they do not exist yet
    if [ ! -d `dirname $word1` ]; then
        mkdir -p $(dirname $word1)
    fi

    #Set up the command
    if [ ${word1:(-1)} == '/' ]; then
        cmd="cp -r"
    fi

    if $update_repo; then
        cmd="$cmd $word2 $word1"        
    else
        cmd="$cmd $word1 $word2"
    fi

    commands[$i]=$cmd
    let "i += 1"
done <install_config

#Double check that the user wants to carry out the update
if $update_repo; then
    echo "You are about to update the repository. Are you sure?"
else
    echo "You are about to update your configuration files. Are you sure?"
fi

select yn in "Yes" "No"; do
    case $yn in
        Yes ) echo "Copying files"; break;;
        No ) echo "Exiting"; exit;;
        * ) echo "Invalid option"; continue;;
    esac
done

ii=1
while [ $ii -lt $i ]; do
    eval ${commands[$ii]}
    let "ii += 1"
done

echo "Finished copying files"
